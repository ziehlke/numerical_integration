import java.util.ArrayList;
import java.util.List;

public class Main {
    private static final int FROM = 0;
    private static final int TO = 15;
    private static final int numberOfDividers = 1000;

    public static void main(String[] args) {

        double sum = 0.0;
        List<Double> points = xPoints(FROM, TO, numberOfDividers);

        for (Double point : points) {
            double y = function(point) * (double) (TO - FROM) / numberOfDividers ;
            sum += y;
        }
        System.out.println(sum);
    }


    private static double function(double x) {
        return (3 * Math.sin(x)) - (0.2 * Math.pow(x, 3)) + (3 * Math.pow(x, 2));
    }


    private static List<Double> xPoints(int from, int to, int numberOfDividers) {
        List<Double> tmp = new ArrayList<>();
        double step = ( (double) (to - from) / numberOfDividers);
        double start = from + step/2;

        for (double i = start; i < to; i += step) {
            tmp.add(i);
        }

        return tmp;
    }
}
